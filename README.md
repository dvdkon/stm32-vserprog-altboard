# stm32-vserprog-altboard

This is an alternative PCB design for use with
[stm32-vserprog](https://github.com/dword1511/stm32-vserprog) firmware.

Changes:

- Changed connector sizing for use with common SOIC8 clips.
- Added option of using external power for flash chip.

The original board design is (C) [dword1511](https://github.com/dword1511)
(Zhang Chi), changes are (C) David Koňařík, all licenced under the GNU GPLv3.
